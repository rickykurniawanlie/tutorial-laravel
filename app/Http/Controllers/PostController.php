<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function submitPost(Request $request) {
    	$post = new Post();
    	$post->title = $request->input('judul');
    	$post->author = $request->input('pengarang');
    	$post->email = $request->input('email');
    	$post->content = $request->input('content');
    	$post->save();
		return redirect()->route('post.home');
    }

    public function home() {
    	$allPosts = Post::get();

    	return view('form', [
    		'posts' => $allPosts
    	]);
    }

    public function test() {
    	$post = Post::where('id', '=', '1')
    			    ->orderBy(['judul','pengarang'])
    			    ->take(10)
    			    ->offset(10)
    			    ->get();
    }
}
