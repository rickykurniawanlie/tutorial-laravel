<!DOCTYPE html>
<html lang="en">
  <head>

  </head>
  <body>
    <table>
      <caption>Daftar Post</caption>
      <thead>
        <tr>
          <th>Judul</th>
          <th>Pengarang</th>
          <th>Email</th>
          <th>Konten</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($posts as $post)
          <tr>
            <td> {{ $post->title }} </td>
            <td> {{ $post->author }} </td>
            <td> {{ $post->email }} </td>
            <td> {{ $post->content }} </td>
          </tr>
          @endforeach
      </tbody>
    </table>

    <form action="form" method="post" accept-charset="utf-8">
         {{ csrf_field() }}
        <input type="text" name="judul" value="" placeholder="Post Title">
        <input type="text" name="pengarang" value="" placeholder="Your Name">
        <input type="email" name="email" value="" placeholder="Your Email">
        <textarea name="content" placeholder="Your content here"> </textarea>
        <input type="submit" name="submit" value="Submit">
    </form>
  </body>
</html>